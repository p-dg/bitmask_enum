// This source file is part of the bitmask_enum library.
// For the latest info, see https://bitbucket.org/p-dg/bitmask_enum
//
// Copyright (c) Paolo Di Giglio
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#pragma once

#include "support_macros.h"
#include <type_traits>

namespace bitmask_enum
{
	template <typename Enum>
	struct enable_bitmask_operators
		: public std::false_type { };

	// TODO: this type traits exists in std since C++23
	template <typename Enum>
	struct is_scoped_enum
		: public std::integral_constant<bool, std::is_enum<Enum>::value && !std::is_convertible<Enum, typename std::underlying_type<Enum>::type>::value> { };

	template <typename Enum>
	struct is_bitmask_enum
		: public std::integral_constant<bool, is_scoped_enum<Enum>::value && enable_bitmask_operators<Enum>::value> { };

	template <typename Enum>
	BTM_ENUM_NODISCARD BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum>::type operator&(Enum lhs, Enum rhs) BTM_ENUM_NOEXCEPT
	{
		using underlying_t = typename std::underlying_type<Enum>::type;
		return static_cast<Enum>(static_cast<underlying_t>(lhs) & static_cast<underlying_t>(rhs));
	}

	template <typename Enum>
	BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum&>::type operator&=(Enum& lhs, Enum rhs) BTM_ENUM_NOEXCEPT
	{
		return lhs = lhs & rhs;
	}

	template <typename Enum>
	BTM_ENUM_NODISCARD BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum>::type operator|(Enum lhs, Enum rhs) BTM_ENUM_NOEXCEPT
	{
		using underlying_t = typename std::underlying_type<Enum>::type;
		return static_cast<Enum>(static_cast<underlying_t>(lhs) | static_cast<underlying_t>(rhs));
	}

	template <typename Enum>
	BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum&>::type operator|=(Enum& lhs, Enum rhs) BTM_ENUM_NOEXCEPT
	{
		return lhs = lhs | rhs;
	}

	template <typename Enum>
	BTM_ENUM_NODISCARD BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum>::type operator^(Enum lhs, Enum rhs) BTM_ENUM_NOEXCEPT
	{
		using underlying_t = typename std::underlying_type<Enum>::type;
		return static_cast<Enum>(static_cast<underlying_t>(lhs) ^ static_cast<underlying_t>(rhs));
	}

	template <typename Enum>
	BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum&>::type operator^=(Enum& lhs, Enum rhs) BTM_ENUM_NOEXCEPT
	{
		return lhs = lhs ^ rhs;
	}

	template <typename Enum>
	BTM_ENUM_NODISCARD BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value, Enum>::type operator~(Enum e) BTM_ENUM_NOEXCEPT
	{
		using underlying_t = typename std::underlying_type<Enum>::type;
		return static_cast<Enum>(~static_cast<underlying_t>(e));
	}

	template <typename Enum, typename Integral>
	BTM_ENUM_NODISCARD BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value && std::is_integral<Integral>::value, Enum>::type operator<<(Enum e, Integral shift) BTM_ENUM_NOEXCEPT
	{
		using underlying_t = typename std::underlying_type<Enum>::type;
		return static_cast<Enum>(static_cast<underlying_t>(e) << shift);
	}

	template <typename Enum, typename Integral>
	BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value && std::is_integral<Integral>::value, Enum&>::type operator<<=(Enum& e, Integral shift) BTM_ENUM_NOEXCEPT
	{
		return e = e << shift;
	}

	template <typename Enum, typename Integral>
	BTM_ENUM_NODISCARD BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value && std::is_integral<Integral>::value, Enum>::type operator>>(Enum e, Integral shift) BTM_ENUM_NOEXCEPT
	{
		using underlying_t = typename std::underlying_type<Enum>::type;
		return static_cast<Enum>(static_cast<underlying_t>(e) >> shift);
	}

	template <typename Enum, typename Integral>
	BTM_ENUM_CONSTEXPR typename std::enable_if<is_bitmask_enum<Enum>::value && std::is_integral<Integral>::value, Enum&>::type operator>>=(Enum& e, Integral shift) BTM_ENUM_NOEXCEPT
	{
		return e = e >> shift;
	}
}
