#include "bitmask_enum.h"

#include <cstdio>
#include <exception>

void btm_enum_expect_impl(char const* expression, char const* file, unsigned line)
{
	std::fprintf(stderr, "Assertion failed: '%s'\n", expression);
	std::fprintf(stderr, "File: %s\n", file);
	std::fprintf(stderr, "Line: %u\n", line);

	std::terminate();
}

#define BTM_ENUM_EXPECT(expression) (void)((!!(expression)) || (btm_enum_expect_impl((#expression), __FILE__, (unsigned)(__LINE__)),0))

enum struct FlagScopedEnum
{
	flag0 = 0,
	flag1 = 1 << 0,
	flag2 = 1 << 1
};

template <>
struct bitmask_enum::enable_bitmask_operators<FlagScopedEnum> : public std::true_type{};

enum FlagEnum { };

struct Scope
{
	enum FlagEnum { };
};

int main()
{
	using namespace bitmask_enum;

	// Check traits.
	static_assert(!is_scoped_enum<FlagEnum>::value, "Bug: FlagEnum is not scoped");
	static_assert(!is_scoped_enum<Scope::FlagEnum>::value, "Bug: FlagEnum is not scoped");
	static_assert(is_scoped_enum<FlagScopedEnum>::value, "Bug: FlagScopedEnum is scoped");

	auto flagEnum = FlagScopedEnum::flag1;
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag1) == FlagScopedEnum::flag1);
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag2) == FlagScopedEnum::flag0);

	flagEnum |= FlagScopedEnum::flag2;
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag1) == FlagScopedEnum::flag1);
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag2) == FlagScopedEnum::flag2);

	flagEnum &= ~FlagScopedEnum::flag1;
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag1) == FlagScopedEnum::flag0);
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag2) == FlagScopedEnum::flag2);

	flagEnum ^= FlagScopedEnum::flag1;
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag1) == FlagScopedEnum::flag1);
	BTM_ENUM_EXPECT((flagEnum & FlagScopedEnum::flag2) == FlagScopedEnum::flag2);

	BTM_ENUM_EXPECT((FlagScopedEnum::flag1 << 1) == FlagScopedEnum::flag2);
	BTM_ENUM_EXPECT((FlagScopedEnum::flag2 >> 1) == FlagScopedEnum::flag1);
	BTM_ENUM_EXPECT((FlagScopedEnum::flag2 >> 2) == FlagScopedEnum::flag0);

	return 0;
}