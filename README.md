### Why `bitmask_enum`

Scoped enumerations are _often_ great.
They're not so great as [bitmask types](https://en.cppreference.com/w/cpp/named_req/BitmaskType),
since they don't (nor should) support bitwise operators out of the box.
This means, as a user, you need to implement the bitwise operators yourself; which may become tedious and error prone if you need several bitmask-like scoped enumerations.
 
`bitmask_enum` is a header-only library that aims to solve this problem by
providing:

 * Implementation templates for the following  operators:
    * bitwise and: `operator&` (and `operator&=`);
    * bitwise xor: `operator^` (and `operator^=`);
    * bitwise or: `operator|` (and `operator|=`);
    * bitwise not: `operator~`;
    * left shift: `operator<<` (and `operator<<=`);
    * right shift: `operator>>` (and `operator>>=`);

 * A way to "tag" a scoped-enum as a bitmask type, in order SFINAE out the operators above for all the non-bitmask scoped enums. 

### Example usage

Here's an example on how to get started with the `bitmask_enum` library:


```cpp
// -- BitmaskEnum.h --
#include "bitmask_enum.h"

enum struct BitmaskEnum
{
    empty = 0,
    bit0 = 1 << 0,
    bit1 = 1 << 1,
    bit2 = 1 << 2
};

// Tag BitmaskEnum as a bitmask
template <>
struct bitmask_enum::enable_bitmask_operators<BitmaskEnum>
    : public std::true_type { };

// -- foo.c --
#incude "BitmaskEnum.h"

bool foo(BitmaskEnum bitmask)
{
    // Introduce all the bitmask_enum members in this scope.
    // That's similar to what you'd do with std literals
    using namespace bitmask_enum;

    bool success {false};
    if (bitmask & BitmaskEnum::bit0)
    {
        // ...
    }
    else if (bitmask & BitmaskEnum::bit1)
    {
        // ...
    }

    // ...

    return success;
}
```
